from django.shortcuts import render

from .forms import ContactUsForm


def index(request):
    return render(request, 'index.html')


def temple_history(request):
    return render(request, 'temple_history.html')


def temple_administration(request):
    return render(request, 'temple_administration.html')


def contact_us(request):
    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            form_data = form.cleaned_data
            name = form_data.get('name')
            email = form_data.get('email')
            enquiry = form_data.get('enquiry')
    form = ContactUsForm()

    return render(request, 'contact_us.html', {'contact_form': form})


