from django import forms


class ContactUsForm(forms.Form):
    name = forms.CharField(max_length=25, required=True)
    email = forms.EmailField(max_length=25, required=True)
    enquiry = forms.CharField(min_length=10, max_length=200, required=True, widget=forms.Textarea)

    name.widget.attrs.update({'class': 'form-control'})
    email.widget.attrs.update({'class': 'form-control'})
    enquiry.widget.attrs.update({'class': 'form-control'})