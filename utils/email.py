from django.core.mail import EmailMessage


class Email:

    def __init__(self):
        pass

    @staticmethod
    def send(*args, **kwargs):
        email_from = "anoopksreyas@gmail.com"
        email = EmailMessage(kwargs.get('subject'), kwargs.get('message'), email_from, kwargs.get('to_list'))
        email.attach(args[0].name, args[0].read(), args[0].content_type)
        email.send(fail_silently=False)
        # send_mail(kwargs.get('subject'), kwargs.get('message'), email_from, kwargs.get('to_list'), fail_silently=False)
        return True
