import re
from django import forms


class MarriageBooking(forms.Form):
    name = forms.CharField(max_length=20, required=True)
    contact_number = forms.CharField(required=True)
    marriage_date = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'], required=True)
    wedding_card = forms.FileField(required=True)

    name.widget.attrs.update({'class': 'form-control'})
    contact_number.widget.attrs.update({'class': 'form-control'})
    marriage_date.widget.attrs.update({'class': 'form-control'})
    wedding_card.widget.attrs.update({'class': 'form-control'})

    def clean_wedding_card(self):
        file = self.cleaned_data['wedding_card']
        file_ext = file.name.split('.')[1]
        if file_ext.lower() not in ['jpg', 'jpeg', 'bmp', 'png', 'pdf', 'doc']:
            raise forms.ValidationError('File format is not valid!')
        return file

    def clean_contact_number(self):
        user_input = self.cleaned_data['contact_number']
        pattern = r'^[+0-9\s]{10,14}$'
        if re.match(pattern, user_input):
            return user_input
        raise forms.ValidationError('Not a valid contact number!')




