from django.shortcuts import render
from .forms import MarriageBooking
from utils.email import Email


def booking_home(request):
    return render(request, 'booking_home.html')


def marriage_booking(request):
    email_template = '''Hi Admin, \n\n You have received a booking for marriage hall. Details below.\n
    Name: {}\n
    Contact number: {} \n
    Marriage date: {} \n
    Wedding card is attached FYR. \n\n\n
    Thanks,\n
    Webmaster
    '''
    message = ''
    error_form = ''
    if request.method == 'POST':
        form = MarriageBooking(request.POST, request.FILES)
        if form.is_valid():
            try:
                form_data = form.cleaned_data
                email_body = email_template.format(form_data.get('name'), form_data.get('contact_number'),
                                                   form_data.get('marriage_date'))
                attachment = form_data.get('wedding_card')
                Email.send(attachment, subject='Marriage Hall Booking', message=email_body,
                           to_list=['anoopksreyas@gmail.com'])
                message = "Marriage Hall Booking submitted."
            except Exception as e:
                print(str(e))
                message = "Booking could n\'t be completed!"
        else:
            print(form.errors)
            error_form = form
    form = MarriageBooking()
    return render(request, 'marriage_booking.html', {'marriage_form': form, 'message': message,
                                                     'error_form': error_form})

