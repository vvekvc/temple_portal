from django.urls import path
from . import views as booking_views

app_name = 'bookings'

urlpatterns = [
    path('', booking_views.booking_home, name='booking-home'),
    path('marriage-hall', booking_views.marriage_booking, name='marriage-booking'),
]