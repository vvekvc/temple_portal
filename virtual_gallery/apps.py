from django.apps import AppConfig


class VirtualGalleryConfig(AppConfig):
    name = 'virtual_gallery'
